
const express = require('express');

const router = express.Router();

const taskController = require('../controller/taskController');



router.post('/', (req,res) => {
	taskController.createTask(req.body).then(result => res.send(result))
})






router.get('/:id', (req, res) => {
	
	taskController.getSpecificTask(req.params.id).then(result => res.send(result));
})






router.put('/:id/complete', (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})


module.exports = router;